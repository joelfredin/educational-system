class Teacher : Person
{
    protected:
        std::string department;
        int salary;

    public:
        Teacher(std::string firstName, std::string lastName, std::string department, int salary):Person(firstName, lastName){
            this->department = department;
            this->salary = salary;
        }
    std::string GetName() {
        return firstName + " " + lastName;
    }
};