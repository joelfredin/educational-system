#include<string>

class Person
{
protected:
    std::string firstName;
    std::string lastName;

public:
    Person(std::string firstName, std::string lastName)
    {
        this->firstName = firstName;
        this->lastName = lastName;
    }

};