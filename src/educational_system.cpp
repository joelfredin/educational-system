#include<iostream>
#include<string>
#include<vector>
#include <fstream>
#include "./hps/src/hps.h"
#include "Course.h"
#include "Person.h"
#include "Student.h"
#include "Teacher.h"

std::vector<Course> construct_list_of_courses()
{
    std::string stopper;
    std::string course_constructor;
    int i = 0;
    std::vector<Course> all_courses;

    while(true)
    {
        Course Course1;
        std::cout << "Do you want to continue adding courses? If not, type NO" << std::endl;
        std::cin >> stopper;
        if(stopper == "NO")
        {
            break;
        }
        std::cout << "Let's add your " + std::to_string(i+1) + "th course!" << std::endl;
        std::cout << "Please, tell me the name of the course: " << std::endl;
        std::cin >> course_constructor;
        Course1.name = course_constructor;
        std::cin >> course_constructor;
        Course1.field_of_study = course_constructor;
        Course1.year = i;
        all_courses.push_back(Course1);
        
        i++;

    }
    return all_courses;
}

std::vector<Student> construct_list_of_students()
{
    std::string stopper;
    std::string person_constructor;
    int j = 0;
    std::vector<Student> all_students;
    std::cout << "Let us now add some students. I let you choose their name, and what courses they shall take.";
    while(true)
    {
        Student student1;
        std::cout << "Do you want to continue adding students? If not, type NO" << std::endl;
        std::cin >> stopper;
        if(stopper == "NO")
        {
            break;
        }
        std::cout << "Let's add your " + std::to_string(j+1) + "th person!" << std::endl;
        std::cout << "Please, tell me the first name of the person " << std::endl;
        std::cin >> person_constructor;
        student1.firstName = person_constructor;
        std::cout << "Now, tell me the last name of that person " << std::endl;
        std::cin >> person_constructor;
        student1.lastName = person_constructor;
        student1.id = j;
        all_students.push_back(student1);
        
        j++;

    }
    return all_students;
}

std::vector<Teacher> construct_list_of_teachers()
{
     std::string stopper;
    std::string teacher_constructor;
    int j = 0;
    std::vector<Teacher> all_teachers;
    std::cout << "Lastly, Let us add some teachers. I let you choose their name, and what courses they are bosses of.";
    while(true)
    {
        int k = 0;
        Teacher teacher1;
        std::cout << "Do you want to continue adding teachers? If not, type NO";
        std::cin >> stopper;
        if(stopper == "NO")
        {
            break;
        }
        std::cout << "Let's add your " + std::to_string(k+1) + "th person!" << std::endl;
        std::cout << "Please, tell me the first name of the person" << std::endl;
        std::cin >> teacher_constructor;
        teacher1.firstName = teacher_constructor;
        std::cout << "Now, tell me the last name of that person" << std::endl;
        std::cin >> teacher_constructor;
        teacher1.lastName = teacher_constructor;
        all_teachers.push_back(teacher1);
        
        k++;

    }
    return all_teachers;
}

std::vector<std::pair<std::string, std::pair<std::string, int>>> construct_vector_of_attributes_courses()
{
    std::vector<std::pair<std::string, std::pair<std::string, int>>> vector_of_attributes;
    //std::vector<Student> student_list = construct_list_of_students()
    for(Course course : construct_list_of_courses())
    {
        vector_of_attributes.push_back({course.name, {course.field_of_study, course.year}});
    }
    return vector_of_attributes;
}

std::vector<std::pair<std::string, std::pair<std::string, int>>> construct_vector_of_attributes_students()
{
    std::vector<std::pair<std::string, std::pair<std::string, int>>> vector_of_attributes;
    //std::vector<Student> student_list = construct_list_of_students()
    for(Student student : construct_list_of_students())
    {
        vector_of_attributes.push_back({student.firstName, {student.lastName, student.id}});
    }
    return vector_of_attributes;
}

std::vector<std::pair<std::string, std::pair<std::string, int>>> construct_vector_of_attributes_teachers()
{
    std::vector<std::pair<std::string, std::pair<std::string, int>>> vector_of_attributes;
    //std::vector<Student> student_list = construct_list_of_students()
    for(Teacher teacher : construct_list_of_teachers())
    {
        vector_of_attributes.push_back({teacher.firstName, {teacher.lastName, 0}});
    }
    return vector_of_attributes;
}

int main()
{
    std::cout << "Hello, and welcome to the education system. Today, you are the headmaster of this school!" << std::endl << std::endl;

    std::cout << "Let us begin by adding some courses" << std::endl;

    std::vector<Course> list_of_courses = construct_list_of_courses();
    std::vector<Student> list_of_students = construct_list_of_students();
    std::vector<Teacher> list_of_teachers = construct_list_of_teachers();

    std::vector<std::pair<std::string, std::pair<std::string, int>>> vector_attributes_courses = construct_vector_of_attributes_courses();
    std::vector<std::pair<std::string, std::pair<std::string, int>>> vector_attributes_students = construct_vector_of_attributes_students();
    std::vector<std::pair<std::string, std::pair<std::string, int>>> vector_attributes_teachers = construct_vector_of_attributes_teachers();

    std::string serialize_courses = hps::to_string(vector_attributes_courses);
    std::ofstream dataFrame_courses;
    dataFrame_courses.open("courses.txt", std::ios::binary | std::ios::app);
    dataFrame_courses << serialize_courses <<std::endl;
    dataFrame_courses.close();


    std::string serialize_students = hps::to_string(vector_attributes_students);
    std::ofstream dataFrame_students;
    dataFrame_students.open("student.txt", std::ios::binary | std::ios::app);
    dataFrame_students << serialize_students <<std::endl;
    dataFrame_students.close();

    std::string serialize_teacher = hps::to_string(vector_attributes_teachers);
    std::ofstream dataFrame_teacher;
    dataFrame_teacher.open("teacher.txt", std::ios::binary | std::ios::app);
    dataFrame_teacher << serialize_teacher <<std::endl;
    dataFrame_teacher.close();

    std::cout << "Do you want me to print out any of the information to the console? Press (C) for courses, (S) for students and (T) for teachers." << std::endl;
    char input;
    std::cin >> input;
    if(input == 'C')
    {
        std::vector<std::pair<std::string, std::pair<std::string, int>>> data_courses;

        std::string line_courses;

        std::ifstream dataFile_courses;

        std::string filePath_courses = "course.txt";

        dataFile_courses.open(filePath_courses, std::ios::binary);
        if(dataFile_courses.is_open())
        {
            std::string dataRead_courses;
            while(getline(dataFile_courses, line_courses))
            {
            // should only be one line to read!
            data_courses = hps::from_string<std::vector<std::pair<std::string, std::pair<std::string, int>>>>(line_courses);
            }
            dataFile_courses.close();
        }
        else
        {
            std::cout << "File read error!" <<std::endl;
        }

        for( std::pair<std::string, std::pair<std::string, int>> entry : data_courses)
        {
            std::cout << entry.first << " | " << entry.second.first << " | " << entry.second.second << std::endl;
        }
    }
    if(input == 'S')
    {
        std::vector<std::pair<std::string, std::pair<std::string, int>>> data_students;

        std::string line_students;

        std::ifstream dataFile_students;

        std::string filePath_students = "students.txt";

        dataFile_students.open(filePath_students, std::ios::binary);
        if(dataFile_students.is_open())
        {
            std::string dataRead_students;
            while(getline(dataFile_students, line_students))
            {
            // should only be one line to read!
            data_students = hps::from_string<std::vector<std::pair<std::string, std::pair<std::string, int>>>>(line_students);
            }
            dataFile_students.close();
        }
        else
        {
            std::cout << "File read error!" <<std::endl;
        }

        for( std::pair<std::string, std::pair<std::string, int>> entry : data_students)
        {
            std::cout << entry.first << " | " << entry.second.first << " | " << entry.second.second << std::endl;
        }
    }
    if(input == 'T')
    {
        std::vector<std::pair<std::string, std::pair<std::string, int>>> data_teachers;

        std::string line_teachers;

        std::ifstream dataFile_teachers;

        std::string filePath_teachers = "teacher.txt";

        dataFile_teachers.open(filePath_teachers, std::ios::binary);
        if(dataFile_teachers.is_open())
        {
            std::string dataRead_teachers;
            while(getline(dataFile_teachers, line_teachers))
            {
            // should only be one line to read!
            data_teachers = hps::from_string<std::vector<std::pair<std::string, std::pair<std::string, int>>>>(line_teachers);
            }
            dataFile_teachers.close();
        }
        else
        {
            std::cout << "File read error!" <<std::endl;
        }

        for( std::pair<std::string, std::pair<std::string, int>> entry : data_teachers)
        {
            std::cout << entry.first << " | " << entry.second.first << " | " << entry.second.second << std::endl;
        }
    }
    std::cout << "Do you want to see the id for the students, press (S)";
    char student_id;
    std::cin >> student_id;
    if(student_id == 'S')
    {
        
    }

    return 0;
}