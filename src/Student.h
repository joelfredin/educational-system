class Student : Person{
public:
    int id;
    std::string firstName;
    std::string lastName;
    std::vector<Course> my_courses;
};