class Student : Person {
    protected:
        std::string course;

        public:
        Student(std::string firstName, std::string lastName, std::string course):Person(firstName, lastName)
        {
            this->course = course;
        }
};